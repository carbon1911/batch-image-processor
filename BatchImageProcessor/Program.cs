﻿using System;

namespace BatchImageProcessor
{
    /// <summary>
    /// The entry class of the application.
    /// </summary>
    class Program
    {        
        // Just a shorthand for new line.
        private static readonly string nl = Environment.NewLine;

        // Entry point
        static void Main(string[] args)
        {
            try
            {
                new MethodsManager().ApplyOperations(args);
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Prints out the usage message.
        /// </summary>
        public static void Help()
        {
            string help = $"Usage:{nl}{nl}" +
                $"Use following syntax if you specify path to one or more images and possible output path for each input image:{nl}{nl}" +
                "BatchImageProcessor.exe <path\\to\\img1> <path\\to\\img2> <path\\to\\img3> ... <path\\to\\imgN> " +
                "[-o <output\\path\\to\\img1> <output\\path\\to\\img2> <output\\path\\to\\img3> ... <output\\path\\to\\imgN>] " +
                $"-imageProcessingOperation1 [arg1 arg2 ... argn] [-IPOp2 [arg1 arg2 ... argn] -IPOp3 [arg1 arg2 ... argn] ... [args...] -IPOpN [arg1 arg2 ... argn]]{nl}{nl}" +

                $"Example usage:{nl}BatchImageProcessor.exe imgs\\img1.jpg -rotate l; Rotates the image on the path imgs\\img1.jpg 90 degrees to the left and saves it as imgs\\img1_processed.jpg.{nl}" +
                $"BatchImageProcessor.exe imgs\\img1.jpg imgs\\img2.jpg -o out\\img1.jpg imgs\\img2.jpg -rotate l -myBrightness 50; Rotates the images on the paths " +
                $"imgs\\img1.jpg and imgs\\img2.jpg 90 degrees to the left, then increases their brightness by 50 and saves them as imgs\\img1_processed.jpg and imgs\\img2_processed.jpg.{nl}{nl}" +


                $"Use syntax below if you specify a path to a folder which contains images to process. You can optionally include an output path for a folder where processed images will be saved to:{nl}{nl}" +
                "BatchImageProcessor.exe <path\\to\\images folder> [-o <path\\to\\output images folder>] " +
                $"-imageProcessingOperation1 [arg1 arg2 ... argn] [-IPOp2 [arg1 arg2 ... argn] -IPOp3 [arg1 arg2 ... argn] ... [args...] -IPOpN [arg1 arg2 ... argn]]{nl}{nl}" +

                $"Example usage:{nl}BatchImageProcessor.exe imgs -rotate 90; Rotates all images in imgs folder 90 degrees to the right and saves them to imgs folder.{nl}" +
                $"BatchImageProcessor.exe imgs -o out -rotate 90 -myBrightness 50; Rotates all images in imgs folder 90 degrees to the right, then increases their brightness by " +
                $"50 and saves them to out folder.{nl}{nl}" +

                $"BatchImageProcessor.exe -m | --methods - shows all available methods.{nl}{nl}" +
                $"BatchImageProcessor.exe -h | --help - shows this help{nl}{nl}" +

                $"Notes:{nl}If you use -o option, make sure the number of output images is the same as the number of input images.{nl}" +
                $"You must specify one or more image processing operations which will be sequentially applied to each input image.{nl}" +
                $"If you use -h or -m (or their non-abbreviated variant), the rest of the arguments are ignored.{nl}" +
                $"For images with spaces in their names wrap up the name of the image with quotation marks.";
            Console.WriteLine(help);
        }
    }
}