﻿using System.Collections.Generic;

namespace BatchImageProcessor
{
    /// <summary>
    /// Encapsulates method arguments, the purpose of this class is to simplify the work with a dictionary data structure.
    /// </summary>
    public class MethodArguments
    {
        /// <summary>
        /// Mapping for the name of method's argument and its value. It is public due to tests. Rather use accessor.
        /// </summary>
        public Dictionary<string, float> Arguments { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MethodArguments(Dictionary<string, float> arguments)
        {
            Arguments = arguments;
        }

        /// <summary>
        /// Accessor for an argument. Prefer using this to the direct reference to the arguments' dictionary.
        /// </summary>
        /// <param name="argument">Desired arugments' name.</param>
        /// <returns>Arguments' value.</returns>
        public float this[string argument]
        {
            get => Arguments[argument];
        }
    }
}
