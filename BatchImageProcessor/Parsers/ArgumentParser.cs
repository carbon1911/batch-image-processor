﻿using ImageProcessor;
using ImageProcessor.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BatchImageProcessor
{

    /// <summary>
    /// Parses input images and CL arguments.
    /// </summary>
    public static class ArgumentParser
    {
        /// <summary>
        /// Parses single image.
        /// </summary>
        /// <param name="imName">Name of the image to be parsed.</param>
        /// <param name="imageFactory">Object that the image will possibly be loaded into.</param>
        /// <param name="msg">Contains error message if an error occured while parsing the image.</param>
        /// <returns>True if the image was loaded correctly, false otherwise.</returns>
        public static bool ParseSingleImage(string imName
            , ImageFactory imageFactory, ref string msg)
        {
            try
            {
                imageFactory.Load(imName);
                return true;
            }
            catch (FileNotFoundException)
            {
                msg = $"{imName}: no such image.";
            }
            catch (ImageFormatException)
            {
                msg = $"{imName}: unsupported format.";
            }
            catch (Exception e)
            {
                msg = $"Error opening {imName}. {e.Message}";
            }
            return false;
        }

        /// <summary>
        /// Parses command-line arguments. After the method ends, ref parameters contain correct values only if the
        /// method returned true. Otherwise don't rely on the values of ref variables.
        /// </summary>
        /// <param name="args">Command-line arguments.</param>
        /// <param name="availableMethods">Available image processing methods.</param>
        /// <param name="inputImgs">If CL arguments were loaded correctly, contains paths to input images.</param>
        /// <param name="outputImgs">If CL arguments were loaded correctly, contains paths to output images.</param>
        /// <param name="operations">If CL arguments were loaded correctly, contains operations that are to apply to the input images.</param>
        /// <returns>True if arguments were parsed correctly, false otherwise.</returns>
        public static bool ParseArguments(string[] args,
            in Dictionary<string, ImageProcessingMethod> availableMethods,
            ref List<string> inputImgs,
            ref List<string> outputImgs,
            ref List<KeyValuePair<ImageProcessingMethod, MethodArguments>> operations)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Too few arguments, aborting.");
                return false;
            }
            return ParseImages(args,
                out inputImgs,
                out outputImgs,
                out int imagesRelatedArgumentsCount)
                &&
                ParseMethodsChain(args,
                out operations,
                availableMethods,
                imagesRelatedArgumentsCount);
        }

        #region private methods
        private static bool ParseImages(string[] args,
            out List<string> inputImgs,
            out List<string> outputImgs,
            out int imagesRelatedArgumentsCount)
        {
            inputImgs = new List<string>();
            outputImgs = new List<string>();
            imagesRelatedArgumentsCount = 0;

            bool isDirectory;
            try
            {
                isDirectory = File.GetAttributes(args[0]).HasFlag(FileAttributes.Directory);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine($"{args[0]}: no such folder, aborting.");
                return false;
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine($"{args[0]}: no such file, aborting.");
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Following error happened when checking path to {args[0]}: {e.Message}");
                return false;
            }

            if (isDirectory)
            {
                if (args[1] == "-o")
                {
                    // Count "-o".
                    ++imagesRelatedArgumentsCount;

                    try
                    {
                        Directory.CreateDirectory(args[2]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Can't create folder {args[2]}. {e.Message}, aborting.");
                        return false;
                    }

                    // Count output dir.
                    ++imagesRelatedArgumentsCount;
                }
                inputImgs.AddRange(Directory.GetFiles(args[0]));

                if (args[1] == "-o")
                {
                    // If the output directory was provided, create paths to all output images and add them to the output images paths.
                    outputImgs.AddRange(inputImgs.Select(imPath => RenameImageToOutputPath(imPath, args)));
                }
                else
                {
                    FillInOutputPaths(outputImgs, inputImgs);
                }

                // Count input dir.
                ++imagesRelatedArgumentsCount;
            }

            // If the input images were not in a directory,
            else
            { 
                int position = 0;
                while (!args[position].StartsWith("-"))
                {
                    if (File.Exists(args[position]))
                    {
                        inputImgs.Add(args[position]);
                    }
                    else
                    {
                        Console.WriteLine($"{args[position]}: No such file, aborting.");
                        return false;
                    }
                    ++position;
                    if (position == args.Length)
                    {
                        Console.WriteLine("Invalid arguments, possibly no image processing operations provided, aborting.");
                        return false;
                    }
                }
                if (args[position] == "-o")
                {
                    ++position;
                    while (!args[position].StartsWith("-"))
                    {
                        outputImgs.Add(args[position]);
                        ++position;
                        if (position == args.Length)
                        {
                            Console.WriteLine("Invalid arguments, possibly no image processing operations provided, aborting.");
                            return false;
                        }
                    }
                    if (inputImgs.Count != outputImgs.Count)
                    {
                        Console.WriteLine("Number of input and output images is different, aborting.");
                        return false;
                    }
                }
                else
                {
                    FillInOutputPaths(outputImgs, inputImgs);
                }
                imagesRelatedArgumentsCount = position;
            }
            return true;
        }

        private static bool ParseMethodsChain(string[] chain,
            out List<KeyValuePair<ImageProcessingMethod, MethodArguments>> operations,
            Dictionary<string, ImageProcessingMethod> availableMethods,
            int offset)
        {
            operations = new List<KeyValuePair<ImageProcessingMethod, MethodArguments>>();

            if (offset >= chain.Length)
            {
                Console.WriteLine("Invalid image processing operations, aborting.");
                return false;
            }
            int currPosition = offset;
            int operationNr = 1;
            while (currPosition < chain.Length)
            {
                if (availableMethods.TryGetValue(chain[currPosition], out var method))
                {
                    var arguments = method.ValidateArgs(chain, ++currPosition);
                    if (arguments == null)
                    {
                        Console.WriteLine($"Operation number {operationNr} ({method.Name}) with invalid arguments, aborting.");
                        return false;
                    }
                    currPosition += method.NumberOfArgs;
                    operations.Add(new KeyValuePair<ImageProcessingMethod, MethodArguments>(method, new MethodArguments(arguments)));
                }
                else
                {
                    Console.WriteLine(chain[currPosition] + ": no such operation, aborting.");
                    return false;
                }
                ++operationNr;
            }
            return true;
        }

        private static string DefaultRenameImage(string imPath)
        {
            var split = imPath.Split('.');
            split[0] = split[0] + "_processed";
            return string.Join(".", split);
        }

        private static string RenameImageToOutputPath(string imPath, string[] args)
        {
            var split = imPath.Split('\\');
            split[0] = args[2];
            return string.Join("\\", split);
        }

        private static void FillInOutputPaths(List<string> emptyOutPaths, List<string> filledInPaths)
        {
            emptyOutPaths.AddRange(filledInPaths.Select(imName => DefaultRenameImage(imName)));
        }

        #endregion
    }
}
