﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BatchImageProcessor.Parsers
{
    /// <summary>
    /// Parses methods from available_methods.txt file.
    /// </summary>
    public static class MethodsParser
    {

        /// <summary>
        /// Loads methods to availableMethods dictionary from available_methods.txt file.
        /// Correct configuration file contains at least one correctly defined image processing method.
        /// Correctly defined image processing method has form [CLI option name] [name of the class that implements the method]* [number of input arguments].
        /// 
        /// *Reflection is used to find the constructor of the class in runtime.
        /// </summary>
        /// <param name="availableMethods">Data structure to be filled with valid methods.</param>
        /// <param name="availableMethodsPath">Path to methods' configuration file.</param>
        /// <returns>True if at least one image processing method was loaded correctly, false otherwise.</returns>
        public static bool LoadMethods(in Dictionary<string, ImageProcessingMethod> availableMethods,
            string availableMethodsPath = "available_methods.txt"
            )
        {
            try
            {
                using (var sr = File.OpenText(availableMethodsPath))
                {
                    return LoadMethods(availableMethods, sr);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Methods configuration file not found, aborting.");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error opening configuration methods configuration file. {e.Message}");
            }
            return false;
        }

        /// <summary>
        /// Loads methods to availableMethods dictionary from stream that contains image processing methods.
        /// </summary>
        /// <param name="availableMethods">Data structure to be filled with valid methods.</param>
        /// <param name="stream">Stream that contains image processing methods.</param>
        /// <returns>True if at least one image processing method was loaded correctly, false otherwise.</returns>
        public static bool LoadMethods(in Dictionary<string, ImageProcessingMethod> availableMethods,
            StreamReader stream)
        {
            if (availableMethods == null)
            {
                Console.WriteLine("Available methods dictionary not initialized, aborting.");
                return false;
            }
            int nrLine = 1;
            string line;
            while ((line = stream.ReadLine()) != null)
            {
                string[] methodInfo = line.Split();
                if (methodInfo.Length != 3)
                {
                    Console.WriteLine($"Invalid method on line {nrLine}, skipping.");

                    // I know that goto is devilish but I find it quite appropriate in this case.
                    goto IncCounter;
                }

                if (!methodInfo[0].StartsWith("-"))
                {
                    Console.WriteLine($"{methodInfo[0]} does not start with a dash, skipping.");
                    goto IncCounter;
                }
                var imageProcessingMethodType = Type.GetType($"BatchImageProcessor.IPMethods.{methodInfo[1]}");

                if (imageProcessingMethodType == null)
                {
                    Console.WriteLine($"{methodInfo[0]}, {methodInfo[1]}: no such method, skipping.");
                    goto IncCounter;
                }

                var imageProcessingMethodCtor = imageProcessingMethodType
                    .GetConstructor(new Type[] { typeof(int), typeof(string) });

                if (imageProcessingMethodCtor == null)
                {
                    Console.WriteLine($"Please make sure {methodInfo[0]} implements a constructor that takes" +
                        $"exactly one integer argument, skipping.");
                    goto IncCounter;
                }

                if (!int.TryParse(methodInfo[2], out int parsedNrArgs))
                {
                    Console.WriteLine($"Invalid number of args for {methodInfo[0]}, skipping.");
                    goto IncCounter;
                }
                var imageProcessingMethod = (ImageProcessingMethod)imageProcessingMethodCtor
                    .Invoke(new object[] { parsedNrArgs, methodInfo[1] });
                try
                {
                    availableMethods.Add(methodInfo[0], imageProcessingMethod);
                }
                catch (ArgumentException)
                {
                    Console.WriteLine($"{methodInfo[0]} already exists, skipping.");
                }
            IncCounter:
                ++nrLine;
            }
            if (availableMethods.Count == 0)
            {
                Console.WriteLine("No image processing method was loaded successfully, aborting.");
                return false;
            }
            return true;
        }
    }
}
