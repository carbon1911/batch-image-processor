﻿using ImageProcessor;
using System.Collections.Generic;

namespace BatchImageProcessor
{
    /// <summary>
    /// Abstract class that implementations of particular image processing methods should inherit from.
    /// Encapsulates <see cref="MethodArguments"/>. Enables validation of input arguments
    /// and application of the image processing method to an image using <see cref="ImageFactory"/> class.
    /// </summary>
    public abstract class ImageProcessingMethod
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="numberOfArgs">Number of input arguments used for user input validation.</param>
        /// <param name="methodName">Name of the method. Used for easier debugging.</param>
        public ImageProcessingMethod(int numberOfArgs, string methodName)
        {
            NumberOfArgs = numberOfArgs;
            Name = methodName;
        }

        /// <summary>
        /// Number of input arguments used for user input validation.
        /// </summary>
        public int NumberOfArgs { get; private set; }

        /// <summary>
        /// Name of the method. Used for easier debugging.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Validates user input arguments.
        /// </summary>
        /// <param name="arguments">Command-line arguments.</param>
        /// <param name="position">Position on which validation of command-line arguments should commence.</param>
        /// <returns>Must return null if parsing failed, otherwise Dictionary<string, float> of names of the arguments and their
        /// corresponding values. The returned dictionary must contain correct number of input arguments.</returns>
        public abstract Dictionary<string, float> ValidateArgs(in string[] arguments, int position);

        /// <summary>
        /// Applies the method to the input image.
        /// </summary>
        /// <param name="imageFactory">Class that encapsulates the input image.
        /// <see cref="https://imageprocessor.org/imageprocessor/imagefactory/"/> link or <see cref="IPMethods.MyBrightness"/>
        /// implementation for basic work with the input image.</param>
        /// <param name="arguments">Parsed user arguments. I.e. correct number of arguments but with unknown values, which may
        /// or may not be wrong.</param>
        public abstract void Apply(ImageFactory imageFactory, MethodArguments arguments);
    }
}
