﻿using ImageProcessor;
using System.Collections.Generic;   

namespace BatchImageProcessor.IPMethods
{
    /// <summary>
    /// Rotates the input image. One can use argument -l to rotate the image to the left by 90 degress, -r respectively or 
    /// -u to rotate the image upside down.
    /// </summary>
    public class Rotate : ImageProcessingMethod
    {
        /// <summary>
        /// Creates the image processing method.
        /// </summary>
        /// <param name="nrArgs">Number of input arguments.</param>
        /// <param name="methodName">Name of the method.</param>
        public Rotate(int nrArgs, string methodName) : base(nrArgs, methodName) { }

        /// <summary>
        /// Applies the method to the input image. Uses the Rotate method of ImageProcessor library.
        /// </summary>
        /// <param name="imageFactory">Class that encapsulates the input image.
        /// <param name="arguments">Parsed user arguments.</param>
        public override void Apply(ImageFactory imageFactory, MethodArguments arguments)
        {
            imageFactory.Rotate(arguments["angles"]);            
        }

        /// <summary>
        /// Validates user input arguments. One can use argument -l to rotate the image to the left by 90 degress,
        /// -r respectively or -u to rotate the image upside down.
        /// </summary>
        /// <param name="arguments">Command-line arguments.</param>
        /// <param name="position">Position on which validation of command-line arguments should commence.</param>
        /// <returns>Must return null if parsing failed, otherwise Dictionary<string, float> of names of the arguments and their
        /// corresponding values. The dictionary contains single entry.</returns>
        public override Dictionary<string, float> ValidateArgs(in string[] arguments, int position)
        {
            if (arguments.Length - position < NumberOfArgs)
            {
                return null;
            }

            if (arguments[position].ToLower() == "l")
            {
                return new Dictionary<string, float>{ { "angles", -90.0f } };
            }
            if (arguments[position].ToLower() == "r")
            {                
                return new Dictionary<string, float> { { "angles", 90.0f } };
            }
            if (arguments[position].ToLower() == "u")
            {
                return new Dictionary<string, float> { { "angles", 180f } };
            }
            if (float.TryParse(arguments[position], out float res))
            {
                return new Dictionary<string, float> { { "angles", res } };
            }
            return null;
        }
    }
}
