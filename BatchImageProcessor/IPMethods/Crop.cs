﻿using ImageProcessor;
using ImageProcessor.Imaging;

using System.Collections.Generic;
using System.Linq;

namespace BatchImageProcessor.IPMethods
{
    /// <summary>
    /// Crops the input image.
    /// </summary>
    public class Crop : ImageProcessingMethod
    {
        /// <summary>
        /// Creates the image processing method.
        /// </summary>
        /// <param name="nrArgs">Number of input arguments.</param>
        /// <param name="methodName">Name of the method.</param>
        public Crop(int nrArgs, string methodName) : base(nrArgs, methodName) { }

        /// <summary>
        /// Applies the method to the input image.
        /// </summary>
        /// <param name="imageFactory">Class that encapsulates the input image.
        /// <param name="arguments">Parsed user arguments. Should contain the coordinates of upper left corner
        /// and lower right corner of the cropping rectangle in pixels.</param>
        public override void Apply(ImageFactory imageFactory, MethodArguments arguments)
        {
            imageFactory.Crop(new CropLayer(
                arguments["left"],
                arguments["top"],
                arguments["right"],
                arguments["bottom"],
                CropMode.Pixels
            ));
        }

        /// <summary>
        /// Validates user input arguments.
        /// </summary>
        /// <param name="arguments">Command-line arguments. Each argument must be positive float to obtain non-null return value.</param>
        /// <param name="position">Position on which validation of command-line arguments should commence.</param>
        /// <returns>Must return null if parsing failed, otherwise Dictionary<string, float> of names of the arguments and their
        /// corresponding values. The dictionary contains 4 entries.</returns>
        public override Dictionary<string, float> ValidateArgs(in string[] arguments, int position)
        {
            if (arguments.Length - position < NumberOfArgs)
            {
                return null;
            }

            var cropArgs = new Dictionary<string, float>();
            foreach (var pair in new string[] { "left", "top", "right", "bottom" }.Select((e, i) => new 
            { 
                side = e,
                index = i
            }))
            {
                if (!float.TryParse(arguments[position + pair.index], out float res) ||
                    res < 0)
                {
                    return null;
                }
                cropArgs.Add(pair.side, res);
            }
            return cropArgs;
        }
    }
}
