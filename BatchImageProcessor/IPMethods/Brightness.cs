﻿using ImageProcessor;
using System.Collections.Generic;

namespace BatchImageProcessor.IPMethods
{
    /// <summary>
    /// Changes the brightness of the image in terms of percent. Initial brightness is 50 %.
    /// Values above 50 % make the image brighter, values below 50 % make the image darker.
    /// </summary>
    public class Brightness : ImageProcessingMethod
    {
        /// <summary>
        /// Creates the image processing method.
        /// </summary>
        /// <param name="nrArgs">Number of input arguments.</param>
        /// <param name="methodName">Name of the method.</param>
        public Brightness(int nrArgs, string name) : base(nrArgs, name) { }

        /// <summary>
        /// Applies the method to the input image.
        /// </summary>
        /// <param name="imageFactory">Class that encapsulates the input image.
        /// <param name="arguments">Parsed user arguments.</param>
        public override void Apply(ImageFactory imageFactory, MethodArguments arguments)
        {
            imageFactory.Brightness((int)arguments["percent"]);
        }

        /// <summary>
        /// Validates user input arguments.
        /// </summary>
        /// <param name="arguments">Command-line arguments.</param>
        /// <param name="position">Position on which validation of command-line arguments should commence.</param>
        /// <returns>Must return null if parsing failed, otherwise Dictionary<string, float> of names of the arguments and their
        /// corresponding values. The dictionary contains single entry.</returns>
        public override Dictionary<string, float> ValidateArgs(in string[] arguments, int position)
        {
            if (arguments.Length - position < NumberOfArgs)
            {
                return null;
            }
            if (float.TryParse(arguments[position], out float res))
            {
                return new Dictionary<string, float> { { "percent", res } };
            }
            return null;
        }
    }
}
