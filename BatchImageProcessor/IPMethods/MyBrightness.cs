﻿using ImageProcessor;
using System.Collections.Generic;
using System.Drawing;

namespace BatchImageProcessor.IPMethods
{
    /// <summary>
    /// Increments or decrements the brightness of the image. This class demonstrates how to work with the input image
    /// without the usage of the methods from <see cref="ImageFactory"/>, i.e. how could possibly look an implementation of
    /// user-defined method.
    /// </summary>
    public class MyBrightness : ImageProcessingMethod
    {
        /// <summary>
        /// Creates the image processing method.
        /// </summary>
        /// <param name="nrArgs">Number of input arguments.</param>
        /// <param name="methodName">Name of the method.</param>
        public MyBrightness(int nrArgs, string name) : base(nrArgs, name) { }

        /// <summary>
        /// Applies the method to the input image. This method demonstrates how to work with the input image
        /// without the usage of the methods from <see cref="ImageFactory"/>, i.e. how could possibly look an implementation of
        /// user-defined method.
        /// </summary>
        /// <param name="imageFactory">Class that encapsulates the input image.
        /// <param name="arguments">Parsed user arguments.</param>
        public override void Apply(ImageFactory imageFactory, MethodArguments arguments)
        {
            using (Bitmap bmp = new Bitmap(imageFactory.Image))
            {
                for (int j = 0; j < bmp.Height; ++j)
                {
                    for (int i = 0; i < bmp.Width; ++i)
                    {
                        Color currColor = bmp.GetPixel(i, j);
                        Color newColor = Color.FromArgb(
                            Clamp(currColor.R + (int)arguments["value"], 0, 255),
                            Clamp(currColor.G + (int)arguments["value"], 0, 255),
                            Clamp(currColor.B + (int)arguments["value"], 0, 255)
                        );
                        bmp.SetPixel(i, j, newColor);
                    }
                }
                imageFactory.Load(bmp);
            }            
        }

        /// <summary>
        /// Validates user input arguments. Method processes arbitrary delta brightness value, though it is then clamped
        /// to minimum/maximum channel intensity value if the resulting intensity is out of bounds. (e.g. each channel is 
        /// clamped to 0-255 range for 32-bit RGBA image.)
        /// </summary>
        /// <param name="arguments">Command-line arguments.</param>
        /// <param name="position">Position on which validation of command-line arguments should commence.</param>
        /// <returns>Must return null if parsing failed, otherwise Dictionary<string, float> of names of the arguments and their
        /// corresponding values. The dictionary contains single entry.</returns>
        public override Dictionary<string, float> ValidateArgs(in string[] arguments, int position)
        {
            if (arguments.Length - position < NumberOfArgs)
            {
                return null;
            }
            if (float.TryParse(arguments[position], out float res))
            {
                return new Dictionary<string, float> { { "value", res } };
            }
            return null;
        }

        private static int Clamp(int value, int min, int max)
        {
            if (value < min)
            {
                return min;
            }
            if (value > max)
            {
                return max;
            }
            return value;
        }
    }
}