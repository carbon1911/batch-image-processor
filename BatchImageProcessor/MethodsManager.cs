﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using System.Text;

using Konsole;
using ImageProcessor;

using BatchImageProcessor.Parsers;

namespace BatchImageProcessor
{
    /// <summary>
    /// Encapsulates all image processing methods and is in charge of
    /// application of the methods to the input images.
    /// </summary>
    public class MethodsManager
    {
        public Dictionary<string, ImageProcessingMethod> AvailableMethods { get; private set; } = new Dictionary<string, ImageProcessingMethod>();

        /// <summary>
        /// Loads available image processing methods from file available_methods.txt.
        /// </summary>
        /// <exception cref="System.Configuration.ConfigurationErrorsException">
        /// Thrown when no method was successfully loaded.</exception>
        public MethodsManager()
        {
            if (!MethodsParser.LoadMethods(AvailableMethods))
            {
                throw new ConfigurationErrorsException();
            }
        }

        /// <summary>
        /// This constructor serves mainly for testing purposes.
        /// </summary>
        /// <param name="stream">Text stream that represents the config file</param>
        /// <exception cref="System.Configuration.ConfigurationErrorsException">
        /// Thrown when no method was successfully loaded.</exception>
        public MethodsManager(StreamReader stream)
        {
            if (!MethodsParser.LoadMethods(AvailableMethods, stream))
            {
                throw new ConfigurationErrorsException();
            }
        }

        /// <summary>
        /// Parses input arguments. If the input arguments are incorrect, does nothing.
        /// Otherwise applies image processing method(s) to the input image(s).
        /// </summary>
        /// <param name="args">Command-line arguments.</param>
        public void ApplyOperations(string[] args)
        {
            List<string> inputImgsPaths = null;
            List<string> outputImgsPaths = null;
            List<KeyValuePair<ImageProcessingMethod, MethodArguments>> operations = null;
            if (args.Length < 1)
            {
                Console.WriteLine("Too few arguments, aborting.");
                return;
            }
            if (args[0] == "-h" || args[0] == "--help")
            {
                Program.Help();
                return;
            }
            if (args[0] == "-m" || args[0] == "--methods")
            {
                Console.WriteLine(ToString());
                return;
            }
            if (!ArgumentParser.ParseArguments(args, AvailableMethods, ref inputImgsPaths, ref outputImgsPaths, ref operations))
            {
                return;
            }

            // At this point inputImgsPaths, outputImgsPaths and operations contain correct data.

            int nrErrorMsgs = 0;
            var progbar = new ProgressBar(inputImgsPaths.Count);
            int currFile = 0;

            Parallel.ForEach(inputImgsPaths.Zip(outputImgsPaths, (inputImagePath, outputImagePath) =>
                new {
                    inputPath = inputImagePath,
                    outputPath = outputImagePath
                }),
                (currentImage) =>
            {
                var console = new ThreadsafeWriter();
                ApplyOperationsToSingleImage(currentImage.inputPath, operations, out string msg, currentImage.outputPath);

                Interlocked.Increment(ref currFile);
                progbar.Refresh(currFile, $"Processing file {currentImage.inputPath}.");

                // If there was some error message, output it.
                // No error message is a string of length 0.
                if (msg.Length != 0)
                {
                    Interlocked.Increment(ref nrErrorMsgs);
                    console.PrintAt(0, console.Y + nrErrorMsgs, msg);
                }
            });

            // This is here because when messages are written to the console after the program terminates, the prompt
            // doesn't skip the messages but overwrites them instead, this enforces the cursor to skip
            // to the proper line.
            for (int i = 0; i < nrErrorMsgs; ++i)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Returns string representation of MethodManager instance,
        /// the representation consists of all available methods along with the number of required
        /// arguments for each method.
        /// </summary>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var e in AvailableMethods)
            {
                sb.Append($"{e.Key} with {e.Value.NumberOfArgs} argument(s).{Environment.NewLine}");
            }
            return sb.ToString();
        }

        #region private methods
        private void ApplyOperationsToSingleImage(string imagePath,
            in IEnumerable<KeyValuePair<ImageProcessingMethod, MethodArguments>> operations,
            out string msg,
            string saveName)
        {
            msg = "";
            using (var imageFactory = new ImageFactory())
            { 
                if (!ArgumentParser.ParseSingleImage(imagePath, imageFactory, ref msg))
                { 
                    imageFactory.Dispose();
                    return;
                }

                foreach (var operation in operations)
                {
                    operation.Key.Apply(imageFactory, operation.Value);
                }
                try
                {
                    imageFactory.Save(Path.Combine(Environment.CurrentDirectory, saveName));
                }
                catch (ArgumentException e)
                {
                    msg = $"Image {imagePath} was saved unsuccessfully. {e.Message}";
                }
                catch (Exception e)
                {
                    msg = $"Error while processing {imagePath}. {e.Message}";
                }
            }
        }
        #endregion
    }
}
